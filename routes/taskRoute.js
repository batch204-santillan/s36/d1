/*
	-Contains all the endpoints for our application
	-We separete the routes such that "index.js" only contains the information on the server
*/

const express = require ("express");
// set up the express router module (allows us to access HTTP method middlewares, makes it easer to create ROUTES for our application)
const router = express.Router();

// import the controller function from the "taskController.js" as a module
const taskController = require("../controllers/taskController")

// ROUTES
	// Route to Get all the tasks
	router.get("/", (req, res) =>
		{
			taskController.getAllTasks().then(resultFromController => res.send(resultFromController))	
		});

	// use "module.exports" to export the router object to use in the index.js file
	module.exports = router;

	// Route to create a task
	router.post("/", (req, res) =>
		{
			//check for the content
			console.log(req.body)

			taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))	
			
		});

	// Route to delete a task
	router.delete("/:id", (req, res) =>
		{
			console.log(req.params)

			taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
		});

	// Route to Update a Task
	router.put ("/:id", (req, res) =>
		{
			// console.log (req.params.id);
			// console.log (req.body);
			taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
		});

// ACTIVITY:
	
	// Route for Getting a specific task
	router.get ("/:id", (req, res) => 
		{
			taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
		});

	// Route for changing the status of a task
	router.put ("/updatestat/:id" , (req, res) =>
		{
			taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
		});


