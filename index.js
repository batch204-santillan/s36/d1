// Setup the Dependencies
	const express = require("express");
	const mongoose = require ("mongoose");

// Allows to use all the routes defined in "taskRoute.js"
	const taskRoute = require("./routes/taskRoute");

// Setup the server
	const app = express();
	const port = 3000;
	app.use(express.json());

// database connection
	mongoose.connect ("mongodb+srv://rafsantillan:admin123@batch204-santillan.1nyzknv.mongodb.net/B204-to-dos?retryWrites=true&w=majority" ,
			{
				useNewUrlParser : true,
				useUnifiedTopology: true
			}
		);

	let db = mongoose.connection;
	// if a connection error occured, output in the console
	db.on("error", console.error.bind(console, "Connection Error"));
	// if the conncetion is successfull, output in the console.
	db.once("open", ()=> console.log("We're connected to the cloud database."))

// Add the task route
	app.use("/tasks", taskRoute)
	//localhost:3000/tasks



/*		----------ALWAYS AT THE BOTTOM---------		*/

// server listening
	app.listen(port, ()=> console.log(`Now listening to port ${port}.`));