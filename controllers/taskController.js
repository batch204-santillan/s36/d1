// Controllers contain the functions and the business logic of our Express JS application

// allow us to use the contents if the "Task.js" file in the models folder
const Task = require("../models/Task");

// Controller function for getting all the tasks
	module.exports.getAllTasks = () =>
	{
		return Task.find({}).then (result => 
		{
			return result
		})
	}

// Controller for create
	module.exports.createTask = (requestBody) =>
	{
		let newTask = new Task (
		{
			name: requestBody.name
		})
		return newTask.save().then((task,error) =>
		{
			if (error)
			{
				console.log(error)
				return false 
			}
			else 
			{
				return task
			}
		})
	};

// Controller for delete
	module.exports.deleteTask = (taskId) =>
	{
		return Task.findByIdAndRemove(taskId).then((removedTask,err) =>
		{
			if (err)
			{
				console.log(err);
				return false
			}
			else
			{
				return removedTask
			}
		})
	};

// Controller for Update
	module.exports.updateTask = (taskId, newContent) =>
	{
		console.log (taskId);		
		console.log (newContent);
		return Task.findById(taskId).then((result,error) =>
		{
			if (error)
			{
				console.log (error)
				return false
			}

			result.name = newContent.name;
			return result.save().then((updateTask, saveErr) =>
			{
				if (saveErr)
				{
					console.log (saveErr)
					return false
				}
				else
				{
					return updateTask
				}
			})
		})
	};

// ACTIVITY

// Controller for Getting a specific task
	module.exports.getSpecificTask = (taskId) => 
	{
		return Task.findById(taskId).then(result =>
		{
			return result
		})
	};

// Controller for Updating Status
	module.exports.updateStatus = (taskId, newContent) =>
	{
		return Task.findById(taskId).then((result,error) =>
		{
			if (error)
			{
				console.log (error)
				return false
			}
			result.status = newContent.status;
			return result.save().then((updateStat, saveErr) =>
			{
				if (saveErr)
				{
					console.log (saveErr)
					return false
				}
				else
				{
					return updateStat
				}
			}) 
		})
	};


	